package com.srivastavsamarth.transporterPOD;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;

//Path : http://localhost:8080/TransporterPOD/login
@Path("/login")
public class Login 
{
	public static int net = 0;
	@GET
	//Path : http://localhost/TransporterPOD/login/dologin
	@Path("/dologin")
	@Consumes(MediaType.APPLICATION_JSON)
	// Query parameters are parameters: http://localhost/TranporterPod/login/dologin?tr_code=abc123&tr_mob=9868986898
	
	public String doLogin(@QueryParam("tr_code") String t_code, @QueryParam("tr_mob") String t_mob) throws Exception{
		String response = "";
		if(checkCredentials(t_code, t_mob)){
			response = Utility.constructJSON("login",true);
				net = DBConnection.get_t_id(t_code, t_mob);
				
		}
		else
		{
			response = Utility.constructJSON("login", false, "Incorrect Transporter Code or Mobile number!");
		}
	return response;
	}
	
		
	public boolean checkCredentials(String t_code,String t_mob)
	{
		System.out.println("inside check");
		boolean result = false;
		if (Utility.isNotNull(t_code)&&Utility.isNotNull(t_mob))
		{
			try
			{
				result = DBConnection.checkLogin(t_code,t_mob);
			}
			catch(Exception e)
			{
				result = false;
				e.printStackTrace();
			}
		}
		else
		{
			result = false;
		}
		return result;
	}
	@GET
	@Path("/getdata")
	public String getJdata()
	{

		ArrayList<JSONObject> l1 = null;
		try {
			l1 = DBConnection.getData(net);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		String res=null;
		try
		{
			res = Utility.constructJSON(l1);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return res;
	}
}
