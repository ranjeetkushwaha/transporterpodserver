package com.srivastavsamarth.transporterPOD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection 
{
	@SuppressWarnings("finally")
	public static Connection createConnection() throws Exception 
	{
		Connection con = null;
		try 
		{
			Class.forName(Constants.dbClass);
			con = DriverManager.getConnection(Constants.dbUrl, Constants.dbUser, Constants.dbPwd);
		} 
		catch (Exception e) 
		{
			throw e;
		} 
		finally 
		{
			return con;
		}
	}
	
	public static ArrayList<String> li = new ArrayList<String>();
	public static ArrayList<JSONObject> l2 ;
	public static boolean checkLogin(String Transcode, String TranMob) throws Exception
	{
		boolean isUserAvailable = false;
		Connection dbconn = null;
		try
		{
			try
			{
				dbconn= DBConnection.createConnection();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			Statement st = dbconn.createStatement();
			String query = "select * from Translog where tr_code='"+ Transcode + "' and tr_mob='" + TranMob + "'";
			ResultSet rs = st.executeQuery(query);
			while(rs.next())
			{
				isUserAvailable = true;
				getData(rs.getInt("t_id"));
			}
		}
		catch(SQLException sqle)
		{
			throw sqle;
		}
		catch(Exception e)
		{
			if(dbconn != null)
				dbconn.close();
			throw e;
		}
		finally
		{
			if(dbconn != null)
				dbconn.close();
		}
		return isUserAvailable;
	}

	public static ArrayList<JSONObject> getData(int T) throws Exception
	{
		Connection dbconn = null;
		JSONObject one ;
		try
		{
			try
			{
				dbconn= DBConnection.createConnection();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			Statement st = dbconn.createStatement();
			String query = "select * from TransPOD where t_id="+ T;
			ResultSet rs = st.executeQuery(query);
			l2 = new ArrayList<JSONObject>();
			while(rs.next())
			{
				one = new JSONObject();
				one.put("t_id",rs.getInt(1));
				one.put("inv_id",rs.getString(2));
				one.put("invoice",rs.getString(3));
				one.put("D_Del",rs.getDate(4));
				one.put("Quantity",rs.getInt(5));
				one.put("customer",rs.getString(6));
				one.put("Del_lat",rs.getFloat(7));
				one.put("Del_long",rs.getFloat(8));
				one.put("stat",rs.getBoolean(9));				
				l2.add(one);
			}
				
		}
		catch(SQLException sqle)
		{
			throw sqle;
		}
		catch(Exception e)
		{
			if(dbconn != null)
				dbconn.close();
			throw e;
		}
		finally
		{
			if(dbconn != null)
				dbconn.close();
		}
		return l2;
	}
	public static int get_t_id(String Transcode, String TranMob) throws Exception
	{
		Connection dbconn = null;
		int n=0;
		try
		{
			try
			{
				dbconn= DBConnection.createConnection();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			Statement st = dbconn.createStatement();
			String query = "select * from Translog where tr_code='"+ Transcode + "' and tr_mob='" + TranMob + "'";
			ResultSet rs = st.executeQuery(query);
			while(rs.next())
			{
				n = rs.getInt("t_id");
			}
		}
		catch(SQLException sqle)
		{
			throw sqle;
		}
		catch(Exception e)
		{
			if(dbconn != null)
				dbconn.close();
			throw e;
		}
		finally
		{
			if(dbconn != null)
				dbconn.close();
		}
	
		return n;
	}
}
